package day07_hw;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class User{
	private int id;
	private String name;
	LocalDate birthday;
	public User(int id, String name, LocalDate birthday) {
		this.id = id;
		this.name = name;
		this.birthday = birthday;
	
	
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public LocalDate getBirthday() {
		return birthday;
	}
	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", birthday=" + birthday + "]";
	}
	
}

class sortListById implements Comparator<User>{

	@Override
	public int compare(User o1, User o2) {
		return o1.getId() - o2.getId();
	}
	
}

class sortListByName implements Comparator<User>{
	
	@Override
	public int compare(User o1, User o2) {
		return o1.getName().compareTo(o2.getName());
	}
	
}
class sortListByBirthday implements Comparator<User>{
	
	@Override
	public int compare(User o1, User o2) {
		return o1.getBirthday().compareTo(o2.getBirthday());
	}
	
}


public class Day07_hw {

	public static void main(String[] args) {
		List<User> userList = new ArrayList <User>();
		userList.add(new User(34, "Lily", LocalDate.parse("2001-03-24")));
		userList.add(new User(21, "Mary", LocalDate.parse("2001-03-01")));
		userList.add(new User(18, "Steven", LocalDate.parse("2011-08-15")));
		userList.add(new User(46, "John", LocalDate.parse("2008-07-11")));
		userList.add(new User(57, "Amy", LocalDate.parse("2000-01-30")));
		
		//before sort
		
		userList.forEach(u -> System.out.println(u+ ""));
		System.out.println();
		
		// sort by Id
		Collections.sort(userList, new sortListById());
		userList.forEach(u -> System.out.println(u+ ""));
		System.out.println();
		
		// sort by Name
		Collections.sort(userList, new sortListByName());
		userList.forEach(u -> System.out.println(u+ ""));
		System.out.println();
		
		// sort by Birthday
		Collections.sort(userList, new sortListByBirthday());
		userList.forEach(u -> System.out.println(u+ ""));
	}
}
